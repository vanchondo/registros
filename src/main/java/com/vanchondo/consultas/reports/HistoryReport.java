/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vanchondo.consultas.reports;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import java.io.FileOutputStream;
import com.vanchondo.consultas.pacientes.Paciente;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author vanchondo
 */
public class HistoryReport {

    private static int ITEMS_PER_PAGE = 24;

    private static int FONT_SIZE = 11;
    private static Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, FONT_SIZE,
            Font.NORMAL, BaseColor.BLACK);

    private static Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, FONT_SIZE,
            Font.BOLD, BaseColor.BLACK);

    public static void generate(String filePath, List<Paciente> pacientes) throws Exception {
        Rectangle rectangle = new Rectangle(842f, 595f);
        Document document = new Document(rectangle);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
        document.open();
        addMetaData(document, pacientes.get(0).getUnidad());

        List<List<Paciente>> pacientesByTurno = splitItemsPerTurno(pacientes);
        addContentByTurno(pacientesByTurno, document);

        document.close();
    }

    private static void addContentByTurno(List<List<Paciente>> pacientesByTurno, Document document) throws Exception {
        for (List<Paciente> pacientes : pacientesByTurno) {
            List<List<Paciente>> pacientesByMedico = splitItemsPerMedico(pacientes);
            addContentByMedico(pacientesByMedico, document);
        }
    }

    private static void addContentByMedico(List<List<Paciente>> pacientesByMedico, Document document) throws Exception {
        for (List<Paciente> pacientes : pacientesByMedico) {
            List<List<Paciente>> pages = splitItemsPerPage(pacientes, ITEMS_PER_PAGE);
            addContentPages(pages, document);
        }
    }

    private static void addContentPages(List<List<Paciente>> pages, Document document) throws Exception {
        for (int i = 0; i < pages.size(); i++) {
            List<Paciente> page = pages.get(i);
            if (!page.isEmpty()) {
                Paciente p = page.get(0);
                addPageHeader(document, p.getTurno(), p.getFecha(), p.getUnidad(), p.getMedico());
                PdfPTable table = addTableContent(document, page, i * ITEMS_PER_PAGE);

                if (i == pages.size() - 1) {
                    int total = (i * ITEMS_PER_PAGE) + page.size();
                    addTotal(table, total);
                }

                document.add(table);
                if (i <= pages.size() - 1) {
                    document.newPage();
                }
            }
        }
    }

    private static List<List<Paciente>> splitItemsPerTurno(List<Paciente> pacientes) {
        List<List<Paciente>> pacientesListByTurno = new ArrayList<>(
                pacientes.stream()
                        .collect(Collectors.groupingBy(Paciente::getTurno))
                        .values());

        return pacientesListByTurno;
    }

    private static List<List<Paciente>> splitItemsPerMedico(List<Paciente> pacientes) {
        List<List<Paciente>> pacientesListByMedico = new ArrayList<>(
                pacientes.stream()
                        .collect(Collectors.groupingBy(Paciente::getMedico))
                        .values());

        return pacientesListByMedico;
    }

    private static List<List<Paciente>> splitItemsPerPage(List<Paciente> pacientes, int itemsPerPage) {
        List<List<Paciente>> pacientesListPages = new ArrayList<>();
        if (pacientes.size() <= itemsPerPage) {
            pacientesListPages.add(pacientes);
        } else {
            int index = 0;
            while (index < pacientes.size()) {
                int to = pacientes.size() - 1;
                if (pacientes.size() > index + itemsPerPage) {
                    to = index + itemsPerPage;
                }
                pacientesListPages.add(pacientes.subList(index, to));
                index += itemsPerPage - 1;
            }
        }
        return pacientesListPages;
    }

    private static void addMetaData(Document document, String unidad) {
        document.addTitle("Lista de pacientes");
        document.addSubject(unidad);
    }

    private static void addPageHeader(Document document, String turno, String fecha, String unidad, String medico) throws DocumentException {
        Chunk glue = new Chunk(new VerticalPositionMark());
        PdfPTable table = new PdfPTable(1);
        table.addCell(getCell("Coordinación de Informacion y Análisis Estratégico", boldFont, PdfPCell.ALIGN_CENTER, Rectangle.NO_BORDER));
        table.addCell(getCell("OOADE CHIHUAHUA", boldFont, PdfPCell.ALIGN_CENTER, Rectangle.NO_BORDER));
        
        Phrase p = new Phrase();
        p.add(new Paragraph("TURNO: ", boldFont));
        p.add(new Paragraph(turno, normalFont));
        p.add(glue);

        p.add(new Paragraph("MEDICO: ", boldFont));
        p.add(new Paragraph(medico, normalFont));
        p.add(glue);

        p.add(new Paragraph("UNIDAD: ", boldFont));
        p.add(new Paragraph(unidad, normalFont));
        p.add(glue);

        p.add(new Paragraph("FECHA: ", boldFont));
        p.add(new Paragraph(fecha, normalFont));
        PdfPCell cell = new PdfPCell();
        cell.addElement(p);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        document.add(table);
    }

    private static PdfPTable addTableContent(Document document, List<Paciente> pacientes, int index) throws DocumentException {
        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(100);
        table.setWidths(new float[]{1, 10, 4, 3, 3, 3, 6});

        table.addCell(getEmptyCell());
        PdfPCell conteoTituloCell = getCell("CONTEO DE CONSULTORIO VIRTUAL", boldFont, PdfPCell.ALIGN_CENTER);
        conteoTituloCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(conteoTituloCell);
        addEmptyCells(table, 5);

        table.addCell(getEmptyCell());
        table.addCell(getCell("NO SOSPECHOSO DE COVID-19", boldFont, PdfPCell.ALIGN_CENTER));
        addEmptyCells(table, 5);

        table.addCell(getEmptyCell());
        table.addCell(getCell("Nombre", boldFont, PdfPCell.ALIGN_CENTER));
        table.addCell(getCell("NSS", boldFont, PdfPCell.ALIGN_CENTER));
        table.addCell(getCell("HTA", boldFont, PdfPCell.ALIGN_CENTER));
        table.addCell(getCell("DIABETES", boldFont, PdfPCell.ALIGN_CENTER));
        table.addCell(getCell("OBESIDAD", boldFont, PdfPCell.ALIGN_CENTER));
        table.addCell(getCell("OTROS", boldFont, PdfPCell.ALIGN_CENTER));

        for (Paciente p : pacientes) {
            table.addCell(getCell(++index + "", boldFont, PdfPCell.ALIGN_RIGHT));
            table.addCell(getCell(p.getNombre() + " " + p.getApellidos(), normalFont));
            table.addCell(getCell(p.getNss(), normalFont));
            table.addCell(getCell(p.getHta(), normalFont));
            table.addCell(getCell(p.getDiabetes(), normalFont));
            table.addCell(getCell(p.getObesidad(), normalFont));
            table.addCell(getCell(p.getOtros(), normalFont));
        }

        return table;

    }

    private static void addTotal(PdfPTable table, int total) {
        addEmptyCells(table, 5);

        PdfPCell cell = getCell("TOTAL:", boldFont, PdfPCell.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);

        cell = getCell(total + "", normalFont);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
    }

    private static void addEmptyCells(PdfPTable table, int qty) {
        for (int i = 0; i < qty; i++) {
            table.addCell(getEmptyCell());
        }
    }

    private static PdfPCell getEmptyCell() {
        PdfPCell cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private static PdfPCell getCell(String text, Font font) {
        return getCell(text, font, PdfPCell.ALIGN_LEFT);
    }

    private static PdfPCell getCell(String text, Font font, int align) {
        return getCell(text, font, align, Rectangle.BOX);
    }
    
    private static PdfPCell getCell(String text, Font font, int align, int border) {
        PdfPCell cell = new PdfPCell(new Paragraph(text, font));
        cell.setBorder(border);
        cell.setHorizontalAlignment(align);
        return cell;
    }
}
