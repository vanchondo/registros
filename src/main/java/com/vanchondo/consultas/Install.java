/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vanchondo.consultas;

import com.vanchondo.consultas.db.DBConnection;
import static com.vanchondo.consultas.db.DBConnection.DB_NAME;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributes;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author vanchondo
 */
public class Install {

    public Install() {
        createDB();
    }

    private void createDB() {
        try (DBConnection jdbc = new DBConnection()) {
            if (!areTablesCreated(jdbc)) {
                File dbFile = new File(DB_NAME);
                setHiddenAttrib(Paths.get(DB_NAME));
                
                createTables(jdbc);
            }
        }
    }

    private static void setHiddenAttrib(Path filePath) {
        try {
            DosFileAttributes attr = Files.readAttributes(filePath, DosFileAttributes.class);
            System.out.println(filePath.getFileName() + " Hidden attribute is " + attr.isHidden());
            Files.setAttribute(filePath, "dos:hidden", true);
            attr = Files.readAttributes(filePath, DosFileAttributes.class);
            System.out.println(filePath.getFileName() + " Hidden attribute is " + attr.isHidden());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private boolean areTablesCreated(DBConnection jdbc) {
        boolean created = false;
        try {
            Statement statement = jdbc.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT name FROM sqlite_master WHERE type='table'");
            if (resultSet.next()) {
                created = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return created;
    }

    private void createTables(DBConnection jdbc) {
        System.out.println("Create tables");
        try {
            createPacientesTable(jdbc);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Deleting DB file");
            new File(DB_NAME).delete();
        }
    }

    private void createPacientesTable(DBConnection jdbc) throws SQLException {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE pacientes (");
        sql.append("id INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sql.append("nombre TEXT NOT NULL, ");
        sql.append("apellidos TEXT NOT NULL, ");
        sql.append("nss TEXT NOT NULL, ");
        sql.append("hta TEXT, ");
        sql.append("otros TEXT, ");
        sql.append("diabetes TEXT, ");
        sql.append("obesidad TEXT, ");
        sql.append("fecha DATE, ");
        sql.append("turno TEXT, ");
        sql.append("unidad TEXT, ");
        sql.append("medico TEXT ");
        sql.append(")");
        Statement stmt = jdbc.getConnection().createStatement();
        stmt.executeUpdate(sql.toString());
    }
}
