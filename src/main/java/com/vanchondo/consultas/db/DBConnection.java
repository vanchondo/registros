/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vanchondo.consultas.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author vanchondo
 */
public class DBConnection implements AutoCloseable{
    public static final String DB_NAME = ".consultas.db";
    private Connection connection = null;

    public DBConnection(){
        connect();
    }

    private void connect(){
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public Connection getConnection(){
        return connection;
    }


    @Override
    public void close() {
        System.out.println("Closing database");
        try {
            connection.close();
        }catch(SQLException ex){
            ex.printStackTrace();
        }
    }
    
}
