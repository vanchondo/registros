/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vanchondo.consultas.pacientes;

import com.vanchondo.consultas.db.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vanchondo
 */
public class PacientesRepository {

    public boolean savePaciente(Paciente paciente){
        boolean success = false;
        paciente.setFecha(LocalDate.now().toString());

        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO pacientes ( ");
        sql.append(getPacienteFields());
        sql.append(") VALUES (");
        sql.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?");
        sql.append(")");

        try(DBConnection jdbc = new DBConnection()){
            PreparedStatement stmt = jdbc.getConnection().prepareStatement(sql.toString());
            stmt.setString(1, paciente.getNombre());
            stmt.setString(2, paciente.getApellidos());
            stmt.setString(3, paciente.getNss());
            stmt.setString(4, paciente.getHta());
            stmt.setString(5, paciente.getDiabetes());
            stmt.setString(6, paciente.getObesidad());
            stmt.setString(7, paciente.getOtros());
            stmt.setString(8, paciente.getFecha());
            stmt.setString(9, paciente.getTurno());
            stmt.setString(10, paciente.getUnidad());
            stmt.setString(11, paciente.getMedico());
            success = stmt.executeUpdate() == 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return success;
    }

    public List<Paciente> findAllByDate(LocalDate date){
        List<Paciente> pacientes = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM pacientes ");
        sql.append("WHERE fecha = ? ");
        sql.append("ORDER BY turno, medico, id ASC ");

        try(DBConnection jdbc = new DBConnection()){
            PreparedStatement stmt = jdbc.getConnection().prepareStatement(sql.toString());

            stmt.setString(1, date.toString());
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()){
                pacientes.add(PacienteMapper.map(resultSet));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return pacientes;
    }
    
    public Paciente getLastSaved(){
        Paciente paciente = new Paciente();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM pacientes ");
        sql.append("ORDER BY id DESC");

        try(DBConnection jdbc = new DBConnection()){
            PreparedStatement stmt = jdbc.getConnection().prepareStatement(sql.toString());
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()){
                paciente = PacienteMapper.map(resultSet);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return paciente;
    }

    private static String getPacienteFields(){
        StringBuilder fields = new StringBuilder();
        fields.append("nombre, ");
        fields.append("apellidos, ");
        fields.append("nss, ");
        fields.append("hta, ");
        fields.append("diabetes, ");
        fields.append("obesidad, ");
        fields.append("otros, ");
        fields.append("fecha, ");
        fields.append("turno, ");
        fields.append("unidad, ");
        fields.append("medico ");

        return fields.toString();
    }
}

