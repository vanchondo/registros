/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vanchondo.consultas.pacientes;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author vanchondo
 */
public class PacienteMapper {

    public static Paciente map(ResultSet resultSet){
        Paciente paciente = new Paciente();
        try {
            paciente.setNombre(resultSet.getString("nombre"));
            paciente.setApellidos(resultSet.getString("apellidos"));
            paciente.setNss(resultSet.getString("nss"));
            paciente.setHta(resultSet.getString("hta"));
            paciente.setDiabetes(resultSet.getString("diabetes"));
            paciente.setObesidad(resultSet.getString("obesidad"));
            paciente.setOtros(resultSet.getString("otros"));
            paciente.setFecha(resultSet.getString("fecha"));
            paciente.setTurno(resultSet.getString("turno"));
            paciente.setUnidad(resultSet.getString("unidad"));
            paciente.setMedico(resultSet.getString("medico"));
        }catch (SQLException ex){
            ex.printStackTrace();
        }

        return paciente;
    }
}
