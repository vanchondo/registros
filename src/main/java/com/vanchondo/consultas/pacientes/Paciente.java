/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vanchondo.consultas.pacientes;

import lombok.Data;

/**
 *
 * @author vanchondo
 */
@Data
public class Paciente {

    private String nombre;
    private String apellidos;
    private String nss;
    private String hta;
    private String diabetes;
    private String obesidad;
    private String otros;
    private String fecha;
    private String unidad;
    private String turno;
    private String medico;

}
